# frozen_string_literal: true

require 'gitlab'
require 'yaml'

class Releases
  WWW_PROJECT_ID = 7764

  def current
    releases.find do |release|
      this_year == release_year(release['date']) &&
        this_month == release_month(release['date'])
    end
  end

  def previous
    current_index = releases.find_index do |release|
      this_year == release_year(release['date']) &&
        this_month == release_month(release['date'])
    end
    current_index ? releases[current_index + 1] : nil
  end

  def upcoming
    current_index = releases.find_index do |release|
      this_year == release_year(release['date']) &&
        this_month == release_month(release['date'])
    end
    current_index ? releases[current_index - 1] : nil
  end

  def self.current
    new.current
  end

  def self.previous
    new.previous
  end

  def self.upcoming
    new.upcoming
  end

  private

  def releases
    YAML.safe_load(release_file_contents)
  end

  def release_file_contents
    releases_path = 'data/releases.yml'
    @gitlab = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4/',
      private_token: ENV.fetch('GITLAB_BOT_TOKEN')
    )
    @gitlab.file_contents(WWW_PROJECT_ID, releases_path, 'master')
  end

  def this_month
    Date.today.month
  end

  def this_year
    Date.today.year
  end

  def release_date(date_string)
    Date.parse(date_string)
  end

  def release_month(date_string)
    release_date(date_string).month
  end

  def release_year(date_string)
    release_date(date_string).year
  end
end
