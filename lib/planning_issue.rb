# frozen_string_literal: true

require 'date'
require 'yaml'
require 'erb'
require 'logger'
require 'gitlab'
require_relative 'releases'
require_relative 'team'

class PlanningIssue
  TEAM_CONFIG_FILE = 'teams.yml.erb'
  META_PROJECT_ID = 10619765

  attr_accessor :releases, :logger

  def initialize(releases: Releases)
    @releases = releases
    @logger = Logger.new($stdout, level: Logger::INFO)
  end

  def create
    teams.each do |team|
      @logger.info("Preparing issue for #{team.name} using template #{team.template}")
      issue_info = create_issue(team.title, planning_issue_description(team.template))
      @logger.info("Issue created for #{team.name}")
      puts "Visit: #{issue_info['web_url']}" # rubocop:disable Rails/Output
    end
  end

  def self.create!
    new.create
  end

  def release_date
    Date.parse(upcoming_release['date'])
  end

  def release_version
    upcoming_release['version']
  end

  def upcoming_release_start_date
    Date.parse(current_release['date']) + 1
  end

  def upcoming_release_end_date
    release_date
  end

  private

  def upcoming_release
    @releases.upcoming
  end

  def current_release
    @releases.current
  end

  def previous_release
    @releases.previous
  end

  def teams
    team_config = ERB.new(File.read(TEAM_CONFIG_FILE)).result(binding)
    @logger.debug(team_config.inspect)

    team_hash = YAML.safe_load(team_config, permitted_classes: [Date])
    team_hash.map do |team|
      name, details = *team
      fields = [:template, :title]
      template, title = details.values_at(*fields.map(&:to_s))
      Team.new(name, template, title)
    end
  end

  def planning_issue_description(template_file)
    ERB.new(File.read(template_file)).result(binding)
  end

  def create_issue(title = "Planning Issue", description = "")
    response = gitlab_client.create_issue(META_PROJECT_ID, title, { description: description })
    @logger.debug(response)

    response
  end

  def gitlab_client
    Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4/',
      private_token: ENV.fetch('GITLAB_BOT_TOKEN')
    )
  end
end
