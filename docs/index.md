# Fulfillment Meta Docs

## Tips and Tricks

- [Developer](tips_and_tricks/developer.md)
- [Terminology](tips_and_tricks/terminology.md)
- [Zuora](tips_and_tricks/zuora.md)

## Methodologies

- [Pair Programming](pair_programming.md)
