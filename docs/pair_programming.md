# Pair Programming in Fulfillment

## What is pair programming?

Pair programming is a software development technique where two engineers work together to solve a particular program or build a feature. Generally, one programmer writes code, as the "driver", while the other, serving as the "observer", reviews the code as it is written. The two programmers may switch roles routinely throughout the session.

## Benefits

Pair programming has a wide range of benefits, both for the engineers participating, and for the team as a whole.

- **Collaboration:** Pair programming promotes collaboration and communication between team members. It allows for instant feedback and knowledge sharing.
- **Quality:** With two sets of eyes on the code, bugs can be identified and addressed more quickly. This can lead to higher code quality.
- **Knowledge sharing:** Pair programming provides an effective means for knowledge transfer between team members. This knowledge sharing isn't limited to less experienced engineers, as anyone can learn from their counterparts.
- **Efficiency:** While it may seem counterintuitive, pair programming can enhance productivity in the long run. Mistakes are caught early, leading to less time spent on debugging and maintenance. Early feedback may also limit the amount of time spent pursuing a less favorable path, or helping someone getting unstuck/unblocked.
- **Team Building:** Pair programming fosters a sense of team camaraderie and helps build a shared understanding of a project among team members.
- **Skill Development:** It allows team members to develop a diverse set of skills, as each person brings their unique strengths and perspectives to the collaboration.

## How it works in Fulfillment

In the Fulfillment section, the use of pair programming is encouraged for all the beneficial reasons above. It is a personal choice however, and there are no hard requirements for how and when it is used.

If a merge request is created as a result of a pairing session, this merge request is considered ahead of schedule in the review process. Let's assume the engineer assigned to the issue is the MR author. The other engineer could be considered the initial reviewer. The reviewer could give their approval immediately as they've provided feedback throughout the pairing session. This MR can essentially be assigned to a maintainer for final approval.

When this happens, it's good practice to indicate in the MR description that both engineers paired in the development of the MR.  Both engineers can be assigned to the MR as well.

Some engineers may prefer to have a routine cadence for pair programming, say once a week or twice a month. Others may chose a more informal approach, reaching out to another engineer when pairing is appropriate or most useful.

### Impromtu pairing

You can always ask in the `#s_fulfillment_engineering` Slack channel to see if anyone is able to pair. Be sure to include information about what you'll be pairing on and a link to the issue. It may be helpful to broadcast the pairing request in advance to give others an opportunity to plan for it.

## Which tasks are good to pair on?

Generally, pair programming is an effective way to get unstuck on a task, when you are unsure of how to approach a task, or for any of the [reasons listed above](#benefits). However, here are some ideas for when to use pair programming:

1. Solution implementation
1. Hunting a bug
1. Knowledge share on a particular topic
1. Build a stronger connection with a team member
1. Gain insight into another team member's approach

## Other resources

1. ["Improving pair programming with pairing sessions"](https://about.gitlab.com/blog/2019/08/20/agile-pairing-sessions/) by GitLab Blog
1. ["On Pair Programming"](https://martinfowler.com/articles/on-pair-programming.html) by Martin Fowler
1. ["Pair Programming Explained](https://shopify.engineering/pair-programming-explained) by Shopify
