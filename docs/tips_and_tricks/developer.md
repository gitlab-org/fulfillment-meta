# Developer

[[_TOC_]]

## Git / Workflow

### Update your branch with latest `main` branch

When the `main` branch has progressed since you created your branch and you want the latest `main` in your branch there are a couple of different strategies you can use, with different pros and cons. Before you do any of these, run `git fetch` to update your remote branches.

* Merge the latest `main` into your branch with `git merge origin/main`
    - Pro: Keep an additive workflow and history intact so reviewers can follow what has happened.
    - Con: You'll see `Merge branch 'main' into 'your-branch'` statements in your branch.
    - Con: Updating with `rebase` afterwards is hard.
* Rebase your branch off of the latest `main` with `git rebase origin/main`:
    - Pro: You won't have `Merge branch 'main' into 'your-branch'` commits on your branch.
    - Pro: You get an opportunity to clean up and rework existing commits (see [Rework your local commits](#rework-your-local-commits) for more information)
    - Con: You might overwrite existing commits, especially commits that entered the history outside of your computer! In order to avoid this, developers can use [`git push --force-with-lease`](https://docs.gitlab.com/ee/topics/git/git_rebase.html#force-push) or run `git pull --rebase` prior to rebasing to ensure that all commits made to the branch stay in the branch.

Note: When updating your branch, the risk of hitting conflicts rises with the size of the change (and small changes encourage iteration), it is a good practice to be mindful of the size of your change and the areas you touch.

#### Useful aliases

* `alias rebase_with_main="git fetch;git rebase main"`

### Rework your local commits

If you haven't come across the [the `--interactive` option](https://git-scm.com/docs/git-rebase#Documentation/git-rebase.txt---interactive) for [`git rebase`](https://docs.gitlab.com/ee/topics/git/git_rebase.html) yet, you'll find that it's a powerful tool to rework your commits. Rebase essentially reapplies commits on top of another base commit, so your branch is based off the new commit.

If you need to rework, e.g. the last 3 commits you can run `git rebase -i HEAD~3`. This will open an editor with a plan you can change to rework your commits: 
* `squash` use commit, but meld into previous commit.
* `fixup` like "squash", but discard this commit's log message.
* `reword` commit to amend commit message.

If you want to use a more iterative approach with your local branch, you can consider using `git commit --fixup` / `git rebase --autosquash`, which can help you keep your commits more [atomic](https://www.freshconsulting.com/insights/blog/atomic-commits/). Read more about the [`fixup`/`autosquash` workflow](https://blog.sebastian-daschner.com/entries/git-commit-fixup-autosquash).

### Merge Requests

* If you'd like to create a branch for an issue you are working on, you can click `Create merge request` button which will create a branch with branch name starting with issue number and opens a new MR page for that branch. If you'd like to just create the branch without the MR, you can click on the dropdown and choose `Create branch` instead of `Create merge request and branch` option.

![Create MR](img/create_merge_request.png)

* When your MR is ready for review, follow the guidelines [here](https://docs.gitlab.com/ee/development/code_review.html#requesting-a-review) to request a review.

* Sometimes a pipeline could fail with  `bundler: failed to load command: undercover ... in 'merge_base': object not found - no match for id (<commit_id>)` error. This generally happens when there are too many commits (more than [20](https://gitlab.com/gitlab-org/gitlab/blob/14a161110bb01060a79343a9ba0cd6b8584e24f6/.gitlab-ci.yml#L67-68) on GitLab). This can be resolved by squashing commits to reduce the number of commits on the MR using `rebase`.

### Quick actions

* `/assign me` to assign yourself
* `/assign_reviewer` to assign a reviewer to an MR
* `/label` to add labels

More GitLab actions [here](https://docs.gitlab.com/ee/user/project/quick_actions.html#issues-merge-requests-and-epics)

### Useful aliases
- `alias unstash='git stash apply stash@{0}'` - apply the latest stash without deleting the stash


## GDK

### GDK troubleshooting

* GitLab web app is stuck in loading state with `502` for longer than usual
    - Run `gdk status` to see the status of the various processes. Check if any of the process is down.
    - You can try restarting the process which is down to fix this issue by running `gdk restart <process-name>` (eg. `gdk restart rails-web`, `gdk restart webpack`).
    - You can look at the logs for various processes using `gdk tail` command for that process (eg. `gdk tail rails-web`, `gdk tail webpack`, etc.).
    - Sometimes its worth restarting your machine and then restarting gdk.
* If something is not working as intended on GitLab, try running `gdk reconfigure && gdk restart`
* You can also resolve the puma errors by running `gdk reconfigure && gdk restart`
* More on troubleshooting [here](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/b2de3e4553ebc3a447ef21797188221324fa4f93/doc/troubleshooting.md)

#### Useful aliases

* `alias gupdate='gdk update && gdk reconfigure && gdk restart'`
* `alias grr='gdk reconfigure && gdk restart'`

### Logs

* To see the last few lines for `gdk` , run `gdk tail`, for `rails-web`, run `gdk tail rails-web` and so on for various processes in gdk
* To clear the logs folder in `gitlab` for debugging purposes, run `gdk cleanup` or `rake gitlab:truncate_logs` from gdk folder

### Translation

* When adding strings for [translation](https://docs.gitlab.com/ee/development/i18n/externalization.html) in GitLab, run `bundle exec rails gettext:regenerate` to add the newly added string to `gitlab.pot` file. This also removes any unsed strings.
* `gitlab.pot` file is synced with a translation tool called [Crowdin](https://docs.gitlab.com/ee/development/i18n/merging_translations.html) when changes are merged to `master` for translation.
* Once a month, the translations from Crowdin are synced to GitLab and `gitlab.po` files for various locales are updated

##### Useful aliases

* `alias translate='bundle exec rails gettext:regenerate'` to update `gitlab.pot` file with newly added externalization strings

### Database migration

* Sometimes when pulling latest changes from main, database migrations need to be run by using `bin/rails db:migrate RAILS_ENV=development`.
    - You can add a handy alias - `alias migrate='bin/rails db:migrate RAILS_ENV=development'`

Note: Database migrations will be run automatically as part of the update if `gdk update` is used to update `gitlab` repo.

### Simulate app as Gitlab.com

Certain features are only available on the SaaS version of GitLab. To be able to access them, run `export GITLAB_SIMULATE_SAAS=true` from your `gitlab-development-kit` folder and restart your gdk `gdk restart`


## A very brief outline of how the app is wired up

When a request is made to an URL (a page or an API call):
* if the route exists in `routes.rb`, the corresponding controller is invoked
* the controller is responsible for returning the template or JSON response, etc.
* the template returned by the controller (if not specified explicitly) follows a naming convention - `action_name.html.haml` (eg. `new.html.haml` for `new` action in the controller)
* the page specific bundle for the action/controller is then executed, these files can be found at `app/frontend/packs/pages/{controller_name}/{action_name}.js`
* the file above is responsible for mounting the Vue app on the selector specified in the HAML file

Eg. `renew` route for subscription (`/subscriptions/{subscription_id}/renew`) >  `renew` action in `subscriptions_controller.rb` > `renew.html.haml` > `app/frontend/packs/pages/subscriptions/renew.js`


## Ruby

### Routes

#### Search for routes with a string
* To list all the routes that contain a particular string, run `rails routes | grep "string_to_search"`

#### List all available routes

* You can also view the list of all available routes, their corresponding helpers, HTTP verb, controller/action in an application under `/rails/info/routes` path of your application, Example:
    - http://localhost:3000/rails/info/routes has the list of all available routes in GitLab (if GitLab is set to run on 3000)
    - http://localhost:5000/rails/info/routes has the list of all available routes in Customers Portal (if Customers Portal is set to run on 5000)

#### Adding a new route shows a blank page

If you're adding a new route and the page is blank upon reloading, try restarting the server.

#### Path and URL helpers

Rails provides some automagic path and URL helpers which look like `edit_project_path` (end with `_path` or `_url`) which would return `/project/:id/edit`. Path helpers return path relative to the site root and URL helpers return the absolute path.

More info on routes [here](https://guides.rubyonrails.org/routing.html)

### Find the controller corresponding to a page

Run `document.body.dataset.page` on browser console

* Example: Running the above command from a project on GitLab will return `'projects:show'` which means the corresponding controller is [`ProjectsController`](https://gitlab.com/gitlab-org/gitlab/-/blob/ec7b9c18c47cb9d3980843eb46410aba888ff46b/app/controllers/projects_controller.rb#L152) and action is [`show`](https://gitlab.com/gitlab-org/gitlab/-/blob/ec7b9c18c47cb9d3980843eb46410aba888ff46b/app/controllers/projects_controller.rb#L152)

### Render

* The `render` method in a controller's action can render various types of content, not just a template. It can be used to render `HTML`, `JSON` (for APIs), `XML`, text, file, etc.

* Not explicitly specifying `render` in an action will render the template corresponding to that action `action_name.html.haml`. For example, not specifying render in `renew` action will render `renew.html.haml` template by default.

* To render a partial template `_my_partial_template.html.haml`, use `render "my_partial_template"`

More information on render [here](https://guides.rubyonrails.org/v5.1/layouts_and_rendering.html)

### rails console

[`rails console`](https://guides.rubyonrails.org/command_line.html#bin-rails-console) or `rails c` lets you interact with a rails application from command line. You can use it to update feature flags, query the database, execute a command, etc.

##### Update feature flags on GitLab

Open a rails console from `gitlab`

* To enable a feature flag - run `Feature.enable(:my_feature_flag)`
* To disable a feature flag - run `Feature.disable(:my_feature_flag)`
* To check the status of a feature flag - run `Feature.enabled?(:my_feature_flag)` or `Feature.disabled?(:my_feature_flag)`

##### Query database

You can query/update the database from rails console

For example:

Open a rails console from `customers-gitlab-com`

* Run `Customer.first` to get the first record from the Customer table
* Run `Customer.find_by(email: 'email@email.com')` to find the customer with the specified email id
* Run `Coupon.find_by_code(‘YOUR_COUPON_CODE’).redeemed!` will update the coupon's status to `redeemed`

The definition for Coupon can be found in [`models/coupon.rb`](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/4a1d1c718f408ee3c5edaaaf6e107341c7b3944e/app/models/coupon.rb). `find_by_code` finds the record by key `code`.

Similary you can run `Coupon.find_by_status(‘redeemed’)` to get a coupon with `redeemed` status.

You can find the list of commands you can use [here](https://guides.rubyonrails.org/active_record_querying.html) and the list of tables in `models` folder of the project

##### GUI Database

* Follow [these instructions](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/postgresql.md#gui-access) to access the database for GitLab using GUI.

* To access the database for Customers Portal using GUI, use `localhost` for username, `5432` for port and `payment_app_development` for database.

##### Execute a command

You can also execute commands from rails console

Example:

* Running `Coupons::GenerateService.execute(1, batch_id: 1)` will run the [`execute`](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/66d19919b9336aa2a1e121876df573069a3e93b3/app/services/coupons/generate_service.rb#L10) method of `GenerateService` class in `Coupons` module and generate a coupon code

### Useful aliases

* `alias bi='bundle install'`
* `alias be='bundle exec'`
* `alias ber='bundle exec rspec'`


## API Testing

### REST API

##### GitLab APIs

To test your local GitLab instance's APIs using Postman, you need to be authenticated from Postman. One of the ways to authenticate is to create a Personal Access Token and specify it as a `Bearer Token` in `Auth` tab. Follow [these steps](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) to create a PAT from your local instance.

You can now make API requests from Postman by filling out the required fields such as the HTTP Verb, URL, request body, etc. You should be able make a request to GitLab APIs, eg. http://localhost:3000/api/v4/namespaces

REST and GraphQL API calls from GitLab to Customers Portal are [proxied](https://gitlab.com/gitlab-org/gitlab/-/blob/5b32ac5ce1cb62e3e32f6c4fcd3df939618f1056/ee/lib/gitlab/subscription_portal/client.rb). You can use the methods defined in [`rest.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/5b32ac5ce1cb62e3e32f6c4fcd3df939618f1056/ee/lib/gitlab/subscription_portal/clients/rest.rb) and [`graphql.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/5b32ac5ce1cb62e3e32f6c4fcd3df939618f1056/ee/lib/gitlab/subscription_portal/clients/graphql.rb) to test them.

##### Customers Portal APIs

To test Customers Portal API that don't need authentication using Postman, eg. `states` API can be accessed by specifying `https://localhost:5000/api/countries/NZL/states` as the URL and `GET` as HTTP verb.

*Test subscription update API locally*

Follow the instructions [here](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/927eb324ea2b156f250d537799b45f2079e6b543/doc/api/index.md#test-subscription-update-api-locally) to test subscription update API locally.

### GraphQL API

You can explore GraphQL APIs on the web app at http://localhost:3000/-/graphql-explorer for GitLab and http://localhost:5000/graphiql for Customers Portal.

[Insomnia](https://docs.insomnia.rest/) is a handy tool to test GraphQL APIs, it stores the history of requests made and also has schema/documentation explorer.

The URL to use in Insomnia for GitLab GraphQL APIs is: http://localhost:3000/api/graphql and Customers Portal is http://localhost:5000/graphql

Select `POST` as HTTP verb, `GraphQL` as request body type and paste the request query and pass in query variables if any at the bottom as JSON.


## RubyMine

RubyMine offers a bunch of useful features that come in handy when working on GitLab / Customers Portal or any Ruby / Ruby on Rails project. You can use the trial version to begin with to try RubyMine. If you want a license, follow the instructions [here](https://about.gitlab.com/handbook/tools-and-tips/other-apps/#jetbrains)

### Jump to the action corresponding to a view

Clicking on the below icon in the gutter will take you to its corresponding action

![Shortcut from view](img/ruby_mine_from_view.png)

### Jump to the view corresponding to an action

Clicking on the below icon in the gutter will take you to its corresponding view

![Shortcut from action](img/ruby_mine_from_action.png)

### Running tests

* Clicking on play icon in the gutter will run the tests.

![Run tests](img/ruby_mine_run_tests.png)

* Right click on the icon to run the tests in debug mode.

![Run tests with options](img/ruby_mine_run_with_options.png)

* You can also place breakpoints in files to pause the execution of the test in debug mode. Use watchers to watch for a value during execution similar to Chrome dev tools.

* In the Run results tab, clicking on a test will show the logs for that test and also will highlight the test in the test file.

* You can use Ctrl + R to rerun the tests or Ctrl + Shift + R to run the tests in a test file.

#### Configuration options

The default configuration templates can be edited to suit your needs. For example, you can edit the default configuration template for Jest to have `--watch` as an option by clicking `Edit Configuration Templates` from `Edit Configuration` modal and changing the config for Jest. This allows the tests to run in watch mode when running the tests from gutter. You can also create a configuration other than the default one to be used when running test by clicking on the run (play) icon at the top.

### Debugging a test file

#### RSpec
* Update `context` and `it` with `fcontext`, `xcontext`, `fit`, `xit` to focus or exclude a test from running
* Execute with `-e` to run a test that matches a string
* Run test with `–seed` if you suspect its an ordering issue
    - get the seed number from the log on pipeline and run the test with `–seed <seed_number>`

#### JS Spec

* Update `describe` and `it` with `fdescribe`, `xdescribe`, `fit`, `xit` to focus or exclude a test from running

#### Cucumber

* Run tests with `CHROME_HEADLESS=false` to run it on chrome

### Scratch files

RubyMine has an option to create scratch files in various formats such as JavaScript, HTML, etc. which can be handy if you need to store some WIP content

### Useful Plugins

#### Key Promoter

[Key Promoter X](https://plugins.jetbrains.com/plugin/9792-key-promoter-x) is useful to get accustomed to the new key bindings in RubyMine when switching from a different IDE by showing the key strokes for the action performed using mouse clicks.

#### String Manipulation

[String Manipulation](https://plugins.jetbrains.com/plugin/2162-string-manipulation) can be handy if you find yourself converting between snake_case / camelCase / kebab-case when working with both Ruby and JavaScript. It also has a lot of other string manipulations that could be useful

### Useful key mapping

* `Cmd + E` to search recent files
* `Double Shift` to search everywhere
* `Cmd + D` to duplicate line or selection
* `Ctrl + G` to select the next occurrence of selected text
* `Ctrl + Cmd + G` to select all occurrence of selected text

### Troubleshooting

If you face `Your Ruby version is x.x.x, but your Gemfile specified x.y.z"` when running a command on terminal in RubyMine, make sure the version of `asdf` in `Preferences > Languages & Frameworks > Ruby SDK and Gems` matches the one in `.tool-versions` and `.ruby-version`.


## byebug

[byebug](https://github.com/deivid-rodriguez/byebug) is a debugging tool for Ruby. To use it:

* [GitLab only] Bring the process to foreground using `gdk thin`.
* Add `byebug` where you want to break and perform the action to debug
    - `byebug if @variable==”something”` for conditional breakpoint
* Once you hit in the breakpoint, inspect variables, eg.:
    - for database things, execute `@project.attributes` to inspect the value of `project`
    - for regular things: `@variable`
    - for objects, `@variable.inspect` to return the string representation of the object
* Useful commands:
    - `s` or `step`: step into
    - `n` or `next`: step over
    - `c` or `continue`: continue until next breakpoint or until execution ends
    - `q` or `quit`: quit
    - press “enter” to repeat last command
    - `reload!(‘filename’)` to reload source code
    - `backtrace` to view backtrace

[byebug cheatsheet](https://cheatography.com/yarik/cheat-sheets/byebug/)


## Customers Dot Troubleshooting

Documentation moved to CustomerPortal [README](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/mailers/index.md).


## Mail in Customers Portal

Documentation moved to CustomerPortal [README](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/setup/installation_steps.md#troubleshooting-1).


## Database in Customers Portal

All models can be found [here](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/main/app/models) and all db fields [here](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/db/schema.rb).

Open a `rails dbconsole` to run SQL commands directly from command line


## More useful aliases

* `alias pretty='yarn run lint:prettier:fix'` to prettify files
* `alias lint='yarn run internal:eslint . --parser-options=schema:tmp/tests/graphql/gitlab_schema_apollo.graphql --fix'`to run lint
* `alias rubo='bundle exec rubocop -A'` to run rubocop and fix correctable offences

## Useful links

* [Testing declined payments](https://stripe.com/docs/testing#declined-payments)
* [VCR Tests](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/testing/tests.md)
* [Customers Portal Staging environment](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/testing/staging.md)
