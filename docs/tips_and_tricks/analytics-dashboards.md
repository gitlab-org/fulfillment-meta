# Useful analytics dashboards for engineers

Some analytics dashboards which might be useful to engineers in the fulfillment team.

| Dashboard | URL |
|-----------|-----|
| Browser summary | https://app.periscopedata.com/app/gitlab/508885/Snowplow-Browser-Summary |
| Device type split | https://app.periscopedata.com/app/gitlab/844296/Device-type |
| Engineering KPIs | https://app.periscopedata.com/app/gitlab/463858/Engineering-KPIs |
| Engineering DIB and collaboration metrics | https://app.periscopedata.com/app/gitlab/1035767/WIP:-Fulfillment-Engineering-DIB-and-collaboration-metrics |
| Engineering division MR rate | https://app.periscopedata.com/app/gitlab/686926/Engineering-Division-MR-Rate |
| Fulfillment maintainer ratio | https://app.periscopedata.com/app/gitlab/825250/WIP:-Fulfillment---Mantainer-ratios |
| GitLab user count | https://app.periscopedata.com/app/gitlab/725040/Gitlab-User-Count |
| Merge request types split | https://app.periscopedata.com/app/gitlab/976854/Merge-Request-Types-Detail |
| New GitLab installs by installation type | https://app.periscopedata.com/app/gitlab/428908/New-GitLab-Installs-by-Installation-Type |
| Snowplow summary dashboard | https://app.periscopedata.com/app/gitlab/417669/Snowplow-Summary-Dashboard |


_Some other useful dashboards are available in the fulfillment sub-department page in the handbook - https://about.gitlab.com/handbook/engineering/development/fulfillment/#fulfillment-stage-dashboards_