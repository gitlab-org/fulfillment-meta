# Fulfillment Terminology and Jargon

Common terminology and jargon that you might come across while working with the Fulfillment team at GitLab.

| Term | Description | Additional details |
|-------------|---------|--------------------|
| Amendment | Also called as subscription amendment, it is a way to modify an active subscription in Zuora. Subscription amendment creates a new version of the subscription in Zuora.| https://knowledgecenter.zuora.com/Billing/Subscriptions/Subscriptions/E_Changing_Subscriptions_Amendments |
| Async Issue Update | Engineers provide an update on the issues assigned to them using a standard template (Generally once a week).| https://about.gitlab.com/handbook/engineering/development/fulfillment/#weekly-async-issue-updates|
| Cloud Licensing | A type of license key for customers to access GitLab paid features. Customers copy and paste an activation code to activate their instance, after which their instance connects to GitLab servers to automatically sync subscription information. | https://docs.gitlab.com/ee/administration/license.html |
| Community Program | The program under which we provide GitLab Ultimate (SaaS or SM) at no cost to customers whose projects fall under the category - Open Source, Education or Startup. | https://about.gitlab.com/solutions/open-source/join/ <br/> https://about.gitlab.com/solutions/education/join/ <br/> https://about.gitlab.com/solutions/startups/  |
| CustomersDot | The application that is used by customers to purchase and manage GitLab plans. <br/>  *Note: Purchase of GitLab SaaS plans has been migrated to gitlab.com.*  | https://customers.gitlab.com/ |
| GitLab SaaS   | Subscription which is hosted by GitLab team on the cloud.  |https://docs.gitlab.com/ee/subscriptions/gitlab_com/  <br/> https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/ |
| Namespace | Unique name for a user, group, or subgroup under which a project can be created. | https://docs.gitlab.com/ee/user/group/#namespaces |
| Pajamas | The design system used by GitLab.  | https://design.gitlab.com/ |
| QSR | Stands for "Quarterly Subscription Reconciliation". QSR charges customers quarterly for the extra users added (above the licensed number of users) only for the remaining quarters in the year unlike true-ups which charge customers annually for the entire year if they add extra users anytime during the year. | https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html |
| Refinement | Stage of issue workflow during which the requirements of an issue are taken to a high level implementation plan along with effort estimation (i.e. weight).|https://about.gitlab.com/handbook/engineering/development/fulfillment/#estimation|
| Seat Link | Refers to the sync of a customer's subscription data of a self-managed instance with GitLab. It enables automation for Quarterly Subscription Reconciliations, Subscription Renewals and Subscription Updates (e.g. adding more seats or upgrading GitLab tier). The sync is performed daily at 03:00 UTC or can be triggered manually. | https://docs.gitlab.com/ee/subscriptions/self_managed/#sync-your-subscription-data-with-gitlab |
| Self-Managed | Abbreviated as SM. GitLab instances that are self hosted either on-premise or on cloud.| https://docs.gitlab.com/ee/subscriptions/self_managed/ <br/> https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/|
| Stripe | Payment gateway. | [Docs for test cards](https://stripe.com/docs/testing) |
| SSOT | Acronym for "Single source of truth". | |
| Super Sonics | Super Sonics is the program that introduced new billing and subscription management functionality including Cloud Licensing, Auto Renewal, Quarterly Co-Terms & Operational Data. | https://about.gitlab.com/pricing/faq-improved-billing-and-subscription-management/ |
| True-up| True-up is a one-time charge at the time of annual renewal that accounts for the users added to an instance above the licensed users.| https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html |
| Weight | The attribute which suggests the estimated effort to complete an issue. | https://about.gitlab.com/handbook/engineering/development/fulfillment/#estimation|
| Zuora | The SSOT for subscriptions, customers, and payment methods data.  | https://university.zuora.com/ ([Details for license](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/410)) <br/> https://knowledgecenter.zuora.com/ |
| SFDC | **S**ales**F**orce **D**ot **C**om | [What does SFDC stand for?](https://developer.salesforce.com/forums/?id=906F00000008koWIAQ)
