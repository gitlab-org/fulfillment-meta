[[_TOC_]]

# Milestone Information

* Start date: `2024-XX-XX`
* End date: `2024-XX-XX`
* [**Active Milestone Board**](https://gitlab.com/groups/gitlab-org/-/boards/7400613) 

# :dart: Milestone Summary

### :bug: &nbsp; Priority Bugs

| Category | Issue | Severity | Priority | Status | Notes |
|-----|----|--------|------|------|------|

We also [track common provisioning bugs publicly in the handbook](https://about.gitlab.com/direction/fulfillment/provision/common-provisioning-bugs/).

### :star: Priority Projects

<table>
<tr>
<th>Priority</th>
<th>Epic / Issue</th>
<th>Teams</th>
<th>Goal for the milestone</th>
<th>DRI</th>
<th>Staffing</th>
<th>UX Support</th>
</tr>
<tr>
<td>

:one: ~"priority::1"
</td>
<td>

</td>
<td>

~3412464 ~2492649

</td>
<td>

</td>
<td>

</td>
<td>

</td>
<td>

</td>
</tr>

</table>

Please refer to the below resources for information on the team's current and upcoming priorities:

* For an overall view of longer-term team priorities, see the following resources:
  * [Provision Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&label_name[]=Fulfillment+Roadmap&label_name[]=group::provision&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=ALL&show_labels=false)
  * [FY25 Fulfillment Priorities issue](https://gitlab.com/gitlab-com/Product/-/issues/12843)
* For project-related priorities for this milestone specifically, see issue comments. Product management will leave a comment 1-2 weeks ahead of milestone start.
* For non-project features & maintenance for this milestone specifically, see the ~"Non-project work" column of the [Active Milestone Board]((https://gitlab.com/groups/gitlab-org/-/boards/7400613)) for a stack-ranked issue list.

 ## Provision Tracking System Triage

 Each week of the milestone one engineering team member will be DRI for monitoring Provision failures using [this process](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/provision_tracking_system/failure_monitoring.md). Monitoring is done using the [#provision_failures](https://gitlab.enterprise.slack.com/archives/C049W1SNRV1) Slack channel and [incident issues created in the CustomersDot project for the failed provisions](https://gitlab.com/gitlab-org/customers-gitlab-com/-/incidents).

Monitoring isn't expected beyond the DRI's working hours. Any issues that arise during this time can be addressed on their next working day. Weekends' issues will be handled by the DRI assigned for the following week.

|Week number | From Date | To Date | Team member |
|---|---|---|---|
|Week XX | 2024-XX-XX | 2024-XX-XX   | PERSON |
|Week XX | 2024-XX-XX | 2024-XX-XX   | PERSON |
|Week XX | 2024-XX-XX | 2024-XX-XX   | PERSON |
|Week XX | 2024-XX-XX | 2024-XX-XX   | PERSON |

# Team Time Off

Please add your PTO dates below for this milestone and don't forget to add these to the Fulfillment calendar.

_Note: `Estimated Weight Capacity` is `(Working days in milestone - Total days off) / 2`, rounded down._

| :calendar_spiral: | [Family & Friends :family:](https://about.gitlab.com/company/family-and-friends-day/#upcoming-family-and-friends-days) | Public Holiday :ferris_wheel: | Vacation :island: | Conference :cityscape: | Total days off | Estimated Weight Capacity |
|-------------------|:----------------------------------------------------------------------------------------------------------------------:|:-----------------------------:|:-----------------:|------------------------|:--------------:|---------------------------|
| **Product Manager**     |  |  |  |  |  |  |
| @ppalanikumar           |  |  |  |  |  |  |
| **Engineering Manager** |  |  |  |  |  |  |
| @rhardarson             |  |  |  |  |  |  |
| **Engineers**           |  |  |  |  |  |  |
| @bhrai                  |  |  |  |  |  |  |
| @cwiesner               |  |  |  |  |  |  |
| @div.ya                 |  |  |  |  |  |  |
| @lwanko                 |  |  |  |  |  |  |
| @mtimoustafa            |  |  |  |  |  |  |
| @paulobarros            |  |  |  |  |  |  |
| @qzhaogitlab            |  |  |  |  |  |  | 


# What's Coming Next?
* Here is the [Next Milestone Board](https://gitlab.com/groups/gitlab-org/-/boards/7308601)
* [Provision Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&label_name[]=Fulfillment+Roadmap&label_name[]=group::provision&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=ALL&show_labels=false)
* [FY25 Fulfillment Priorities issue](https://gitlab.com/gitlab-com/Product/-/issues/12843)
* Next milestone draft planning issue:

/label ~"devops::fulfillment" ~"section::fulfillment" ~"group::provision" ~"Planning Issue" ~"product work" ~"type::ignore"
/epic https://gitlab.com/groups/gitlab-org/-/epics/8243

/assign @ppalanikumar @rhardarson
/confidential
