# End to End Testing Training

This training will help you get started with End-to-End (E2E) testing using the CustomersDot Watir framework. This training template is designed to facilitate ownership of quality within development, including E2E testing, and provide a better onboarding experience for GitLab engineers.

## Read

- [ ] [CustomersDot End-to-End Testing Beginners Guide](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/qa/doc/beginners_guide.md)
- [ ] [CustomersDot End-to-End Test Execution](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/qa/doc/running_tests.md)

## Watch

- [ ] [CustomersDot End-to-End Test Suite Overview](https://youtu.be/7tseMIOn7Oo) (66 mins)

## Optional videos and reading

### Helpful Links

- [ ] [Beginner's guide to writing end-to-end tests](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/beginners_guide.html)
- [ ] [Automated test cases](https://gitlab.com/gitlab-org/customers-gitlab-com/-/quality/test_cases?state=opened&sort=created_desc&page=1&label_name%5B%5D=status%3A%3Aautomated)
- [ ] [Quality section in Fulfillment Handbook](https://about.gitlab.com/handbook/engineering/development/fulfillment/#quality)
- [ ] [Testcase Management Labels](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/qa/doc/testcase_management_labels.md)

### E2E test failures in pipelines

- [ ] [How to triage a QA test pipeline failure](https://about.gitlab.com/handbook/engineering/quality/guidelines/debugging-qa-test-failures/#how-to-triage-a-qa-test-pipeline-failure)

### QA infrastructure and tooling

- [ ] [Chemlab Page Library demo and AMA](https://www.youtube.com/watch?v=js3zm8IrwTg) (52 min)
