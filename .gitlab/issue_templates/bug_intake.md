[[_TOC_]]

## Summary: Describe what is the bug?

## Resolution steps
- [ ] Requester fill in the information in this template, including at a minimum the summary and known bug impact. 
- [ ] Requester assigns the bug to the appropriate Fulfillment PM for the impacted group. If unknown, assign to `@courtmeddaugh` for review. 
- [ ] PM creates any necessary issues for the resolution of the underlying issue or bug in the appropriate project (gitlab-com, gitlab-org, etc.)
- [ ] PM coordinates with stakeholders any resolution steps needed and closes this issue.

## Bug impact
(Include any sizing on the number of impacted customers, subscriptions, billings, revenue/ARR. If unknown, work with Fulfillment and the BSA team.)


## Additional Details

**Will this issue or bug impact customer's quarterly subscription reconciliation (QSR) or renewal?**: `YES/NO`
(If YES include impact in customer communication; If NO include impact in customer communication)


**Will bug/issue impact customer subscription?**: `YES/NO`
(If YES provide summary of subscription impact; if NO provide summary of why subscription will not be impacted)


**Will customer require a refund?**: `YES/NO`
(If YES provide reason for refund)

## Process Steps for Customer Communication

This section is important when we need to get input from customers on their preference among various options, and take action based on customer's response. 

- [ ] Draft customer email notification
   - [ ] Review by Director of Product, Fulfillment
   - [ ] <update this section with additional reviews needed, consider impacted groups (Support, Billing, Deal Desk, etc.)>
- [ ] Determine which system will be used to contact customers
- [ ] Determine process or solution to collect customer responses and prefrences
- [ ] Define intake period (deadline by which customers need to respond)
- [ ] Get necessary approval (reference the approval matrix) from required DRIs, based on expected impact
   - [ ] Reference the [Approval Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix) for guidance
- [ ] Send customer notices with solution options/proposal w/date of refund/update/resolution
- [ ] Process customer responses and action on their preferences


