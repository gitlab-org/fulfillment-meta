## Purpose

- Document questions to ask customer during conversations with them

## Questions

**Who**
1. Who is responsible for user management of GitLab? 
2. Is that also the same person who is responsible for cost management of GitLab?

**What**
1. What are some of the common things that you're going into customers.gitlab.com for? How often are you doing this?
2. What are some of the common things you're going into gitlab.com usage quotas page for? How often are you doing?
3. What problem do you have with the experience today?
4. What do you wish was there that wasn't there?
5. Can you walk me through your workflow?

**Additional q's**
1. Why is this important?
2. What visibility is lacking?

**User management specific**
1. How do you invite new users?
2. How do you use the user caps feature? What do you like the feature? What isn't working?
2. How do they promote existing users to an elevated role?
3. Do you have any workflow related to changing/inviting users (management approval/IT desk approval/etc)?
4. Can your instance have personal projects? Is that an issue for them?
5. Do you have external SSoT for user access?
6. What levels of access do they have in the SAML?
7. What happens if GitLab rights mismatch that of SAML?
8. How are using the user caps flows today in conjunction with SAML (assuming they are using SAML) given that user caps have approvals inside of GitLab?
