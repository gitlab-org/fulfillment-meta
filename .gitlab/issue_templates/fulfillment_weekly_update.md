<!--
  Creating the issue:
    1. Create this issue on Monday of the week that the update will be for.
    2. Title format: "Fulfillment Weekly Update: YYYY-MM-DD to YYYY-MM-DD"
      - where the dates are for Monday and Sunday of that week
  After the issue is created:
    1. Check the Due Date - it should be set to Thursday of that week  (there's a command at the bottom of the template)
    2. Check Assignees - it should be assigned to all relevant Fulfillment members (there's a command at the bottom of the template)
    3. Ensure the issue is Confidential - it should be confidnetial due to the nature of our team's work (there's a command at the bottom of the template)
  Creating a video recording: 
    1. Keep the recording to under 5 minutes
    2. Uploaded the recording to GitLab Unfiltered Youtube.
    3. Set the video to private and add the video to the "Fulfillment Weekly Updates" playlist: https://www.youtube.com/watch?v=BlR18ZeiqWQ&list=PL05JrBw4t0KpuMC7NfDzfuQAz6FFS6akL.
  Announcing the weekly update
    1. Create a the announcement message:
      ```
      Hi team, here's our latest [Fulfillment Weekly Update: 2022-01-01 to 2022-01-07](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues?sort=created_date&state=all&label_name[]=weekly+update)

      5 min overview: https://www.youtube.com/watch?v=BlR18ZeiqWQ&list=PL05JrBw4t0KpuMC7NfDzfuQAz6FFS6akL

      **Key Highlights:**
      - Highlight 1
      - Highlight 2
      - Highlight 3
      - Highlight 4
      ```
    2. Announce in #s_fulfillment Slack channel and tag our stakeholders for visibility.
       ```
        cc for visibility:
          Sales: @James Harrison @Jack @Kazem Kutob
          CoST: @marshall007
          IT: @Christopher Nelson @BWise @Daniel Parker
       ```
    4. Cross-post to #cto Slack channel and cc: @Eric Johnson @leif for visibility.
    5. Announce in the Weekly Update Message and tag @fulfillment-group. 
/-->

# Fulfillment Weekly Update: 2022-01-01 to 2022-01-07

All Fulfillment weekly updates can be found [here](https://gitlab.com/gitlab-org/fulfillment-meta/-/issues?sort=created_date&state=all&label_name[]=weekly+update) along with their [recordings](https://www.youtube.com/watch?v=BlR18ZeiqWQ&list=PL05JrBw4t0KpuMC7NfDzfuQAz6FFS6akL).

[[_TOC_]]

## Recording

5 min overview: https://www.youtube.com/watch?v=BlR18ZeiqWQ&list=PL05JrBw4t0KpuMC7NfDzfuQAz6FFS6akL

## Executive and Stakeholder Updates

### What's happening

_Updates about upcoming executive / stakeholder activities, OKRs, incoming asks, and our asks._

- `@Name`: We have an upcoming Executive Status Update on YYYY-MM-DD. Link to draft slides.
- `@Name`: We completed our Development Group Conversation on YYYY-MM-DD. Link to slides / recording.
- `@Name`: We've closed our FY22-Q4 OKRs and have opened our FY23-Q1 OKRs. Link to OKRs.
- `@Name`: We have an incoming ask from [Stakeholder Name] to begin looking into x.
- `@Name`: We are getting sign off from executives/stakeholders on x.

## Product Updates

### What's happening

_Updates about product roadmap, product initiatives, milestone planning, and updates to our project management process_

- `@Name`: We've updated our Direction page with x.
- `@Name`: We've shifted around initiative x, y, z on our Roadmap.
- `@Name`: We've changed our Project Management Process to help us improve x.
- `@Name`: We've started looking into a new initiative.
- `@Name`: We've started planning for milestone x.

### Initiatives

**Roadmap:** 
- https://about.gitlab.com/direction/fulfillment/#roadmap

#### 1. Initiative Name
- Team: [Fulfillment Purchase / License / Utilization]
- % Complete: x%
- Target Date: 2022-01-01
- Workflow Status: [Problem Validation / Design / Solution Validation / Planning Breakdown / Scheduling / Ready for Development / In Dev / In Review / Verification]
- R/Y/G: [Red / Yellow / Green]
- Current status: ...
- What's next: ...
- Risk/Blockers: ...

## Technical Updates

### What's happening

_Updates about technical roadmap, technical initiatives, key changes in developer tooling / infrastructure / testing / etc._

- `@Name`: We've scheduled a maintainence window on YYYY-MM-DD to improve our infrastructure.
- `@Name`: We've noticed our CPU is beginning to spike during x which is likely due to y. Link to investigation issue.
- `@Name`: We've implemented security fix for x vulnerability. 
- `@Name`: We've updated our test suite to address failures in x.
- `@Name`: We've updated x Ruby gem due to failures in x. Please ensure your machines are updated to the latest version.

### Initiatives

#### 1. Initiative Name
- Team: [Fulfillment Purchase / License / Utilization]
- % Complete: x%
- Target Date: 2022-01-01
- Workflow Status: [Problem Validation / Design / Solution Validation / Planning Breakdown / Scheduling / Ready for Development / In Dev / In Review / Verification]
- R/Y/G: [Red / Yellow / Green]
- Current status: ...
- What's next: ...
- Risk/Blockers: ...

## Team Updates

### What's happening

_Updates about new hires, promotions, bonuses, anniversaries, departures, staffing and hiring plans._

- `@Name`: Please welcome [Team Membmer Name] to [Team Name]! [Team Membmer Name] will be joining us as a [Role Name] starting on YYYY-MM-DD.
- `@Name`: Congratulations [Team Membmer Name] on their promotion to [Role Name]!
- `@Name`: People Managers are currently completing x and will review with their direct reports by YYYY-MM-DD. 
- `@Name`: We have x candidates in the offer stage for [Role Name, Team Name].

### Staffing and Hiring

**Staffing and Hiring Plan:** 
- https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/319

**Open Roles**
- `@Hiring Manager`: [Role Name, Team Name](https://boards.greenhouse.io/gitlab/)

**Upcoming Roles**
- `@Hiring Manager`: [Role Name, Team Name](https://boards.greenhouse.io/gitlab/) - Opening in FY23-Q1

**Hiring Pipeline**
- [Role Name, Team Name](https://boards.greenhouse.io/gitlab/) (x open roles, x hired)
  - Screening: x
  - Team Interview: x
  - Reference Check x
  - Justification: x
  - Background Check: x
  - Hired: x

/label ~"weekly update" ~"devops::fulfillment" ~"section::fulfillment"

/assign @jeromezng @jameslopez @rhardarson @csouthard @courtmeddaugh @tgolubeva @doniquesmit @jackib @vincywilson

/due this Thursday

/confidential
