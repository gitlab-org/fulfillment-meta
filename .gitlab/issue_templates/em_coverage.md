<!--

Instructions:

Create a coverage issue early ahead of your scheduled PTO.

Fill out the sections below and mention other team members who can potentially help with a coverage area or task.

Propose revisions to this template with findings from retrospective threads.

Helpful considerations:

- add links to relevant calendar events or agenda docs, or to put the date/time in the description of the coverage responsibility/task
- declare the boundaries of what can/will be deferred and what requires attention
- link to other async issues like weekly updates, milestone planning issues, or other central locations for contextual information

-->

## Intent

This issue describes how appointed DRIs, the `insert group`, and Fulfillment Engineering Managers are able to execute on a subset of tasks while an Engineering Manager is taking meaningful time away from work.

## :handshake: Responsibilities

Responsibilities include reviewing merge requests, responding to questions, providing feedback, responding to Slack messages, escalating concerns, and updating issues. Communicating expectations even potential areas or outcomes is helpful for others providing coverage.

| Priority | Responsibility  | Content/URL  | Primary backup DRI  |  Secondary backup DRI |
|:--|:--|:--|:--|:--|
| HIGH/MEDIUM/LOW | General MGMT coverage: Responding to adjudicating issues for escalation or providing feedback in issues that require immediate attention  | [X.y Planning Issue](#) | Manager/EM/IC | Manager/EM/IC |
| MEDIUM | Access Request approvals  | | skip-level |  |
|  |  |  |  |  |


## :lifter: Coverage Tasks

- [ ] Item - Link - `@assignee`

## :blue_car: Parked Tasks

- I will be deferring all 1-on-1s until after my PTO

## :books: References

- [Communicating Your Time Off](https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off)
- [Group Page](#)
- [Helpful Link](#)
- [Another Helpful Link](#)
- [Relevant Video Discussion](https://youtu.be/dQw4w9WgXcQ)

## :white_check_mark: Issue Tasks

### Opening Tasks

- [ ] Assign to yourself
- [ ] Title the issue `ROLE Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add an issue comment for your retrospective titled: `:recycle: Retrospective Thread`
- [ ] Add any relevant references
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to those tasks
- [ ] Share this issue in your section, stage, and group Slack channels
- [ ] Ensure your Time Off by Deel auto-responder points team members to this issue

### Closing Tasks

- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective items with your counterparts and manager

## Previous Examples

- https://gitlab.com/gitlab-org/utilization-group/team-project/-/issues/13
- https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/196

/assign me