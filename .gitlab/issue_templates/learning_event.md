 <!-- 

 Below are some good examples of how to build out issues like this one

- https://gitlab.com/gitlab-org/create-stage/-/issues/12820
- https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/242
- https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/244
- https://gitlab.com/gitlab-org/fulfillment-meta/-/issues/280


 --> 

## What

A high-level description of the topic covered by this knowledge sharing session.

## Why

To deliver a greater understanding amongst team members about this very interesting topic.

## Who

### Presenter

- TBD

### Attendees

- GitLab team members
- Recording will be made available for later viewing and possibly for those outside of GitLab if content is not confidential. 

## When

August 2021

**Session Events** 

- Wednesday 2021-00-00 at 11am EDT / 3pm UTC | [agenda](#) | [zoom](#) | [recording](#)

## Content

**Topic areas**

1. Any knowledge you wish to share with the team.

## Materials

- Slides (if necessary)
- Relevant Issue and/or Epics 
- Relevant Merge Requests (if appropriate)
- Links to foundational material
   - neat link 1
- [tutorial](https://youtu.be/dQw4w9WgXcQ)

/label ~knowledge-sharing
