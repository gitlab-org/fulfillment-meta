### Welcome to the Fulfillment team! :tada: :tada: :tada:

This issue is meant to guide you over the first few days in the role. Please reach out to your onboarding buddy or manager if you need any help during your onboarding (and afterwards).

## New team member [to be done from the 1st day you join the team]

### New team member

1. [ ] Slack - Make sure you join:
    - [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH) - General Fulfillment section channel
    - [#s_fulfillment_fyi](https://gitlab.slack.com/archives/C042N0EET9N) - Fulfillment announcements
    - [#s_fulfillment_daily](https://app.slack.com/client/T02592416/C01BNLX4085) - Async Daily Standups (uses Geekbot)
    - [#s_fulfillment_status](https://gitlab.slack.com/archives/CL7SX4N86) - Deployments, Sentry errors, and health checks updates
    - [#s_fulfillment_engineering](https://app.slack.com/client/T02592416/C029YFPUA6M) - Fulfillment section engineering discussions
    - [#s_fulfillment_social](https://app.slack.com/client/T02592416/CKFKK46SG) - To chat and socialize with other Fulfillment team members (Verify that your onboarding buddy added you there, if not please ask them to do so)
    - [Key Slack channels to follow](https://handbook.gitlab.com/handbook/communication/chat/#key-slack-channels) if you haven't already
    - More [Slack channels](https://handbook.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/getting-started/keeping-yourself-informed/#slack-channels) from Brand and Product Marketing's channel list.
1. [ ] :wave: Your [onboarding buddy](#onboarding-buddy) will be _Onboarding buddy_. Please reach out for help installing the apps or for anything technical
1. [ ] Setup call with _Onboarding buddy_ to discuss key points about Customer application.
1. [ ] :calendar: Meetings: [Subscribe to the Fulfillment calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com&ctz=Europe%2FMadrid) by clicking on the :heavy_plus_sign: at the bottom right of that page. There's a weekly social with PMs and other counterparts (we chat about random things and how are things outside of GitLab). The rest are 1:1s and similar.
1. [ ] :books: Check out the [direction](https://about.gitlab.com/direction/fulfillment/) of Fulfillment
1. [ ] :tools: Read about how we work in [Fulfillment](https://about.gitlab.com/handbook/engineering/development/fulfillment/#project-management-process)
1. [ ] :tv: Watch [this knowledge sharing session](https://drive.google.com/file/d/1hLF-hbgKblD8uETLFzvL4N1uM9I3PY_c) about the meaning of commonly used GitLab terms like _CE_, _EE_, _SaaS_, and _self-managed_
1. [ ] :tv: Have a look at [Behind the scenes of a purchase flow in the Customer Portal](https://www.youtube.com/watch?v=Y1EC7mCLqiE)
1. [ ] :tv: Check out [a video on how various Fulfillment systems work together from a Backend perspective](https://youtu.be/iPP0vplNSmY) (use [GitLab Unfiltered](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#channels) account to view the video, and remember to switch back to your own youtube profile)
1. [ ] :books: Read about [subscriptions](https://docs.gitlab.com/ee/subscriptions/) and [licensing](https://about.gitlab.com/pricing/licensing-faq/) FAQ.
1. [ ] :books: Read the [development documentation for the customers app](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/main/doc#development-documentation)
1. [ ] If not already provisioned, following the steps [listed here](https://internal.gitlab.com/handbook/it/end-user-services/access-request/baseline-entitlements/#how-can-i-create-a-new-ar-using-a-baseline-template), [create an access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new) using one of these templates:
   - [Fulfillment Backend Engineer](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_core_development/role_backend_engineer_fulfillment.md)
   - [Fulfillment Frontend Engineer](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_core_development/role_frontend_engineer_fulfillment.md)
1. [ ] :books: Read the [CustomersDot README](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/README.md)
1. [ ] :computer: Download and install:
      1. [ ] [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/setup/installation_steps.md)
1. [ ] :books: Read up on the release process for [GitLab.com (SaaS)](https://handbook.gitlab.com/handbook/engineering/deployments-and-releases/deployments/) and [GitLab self-managed](https://handbook.gitlab.com/handbook/engineering/releases)
1. [ ] :books: Read up on the release process for [CustomersDot](https://handbook.gitlab.com/handbook/engineering/development/fulfillment/#deployment)
1. [ ] :books: Complete the required [reliability training](https://levelup.gitlab.com/courses/development-reliability)
1. [ ] :tools: If you need to run the GitLab EE edition on your local environment, request a [GitLab EE developer license](https://handbook.gitlab.com/handbook/engineering/developer-onboarding/#working-on-gitlab-ee-developer-licenses) and configure it as described on the [documentation page](https://docs.gitlab.com/ee/user/admin_area/license_file.html).
1. [ ] :coffee: Take sometime to setup [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with the [Fulfillment team members](https://about.gitlab.com/handbook/engineering/development/fulfillment/#want-to-know-more-about-us)
      1. [ ] Coffee Chat with ___
      1. [ ] Coffee Chat with ___
      1. [ ] Coffee Chat with ___
      - You can also use [#s_fulfillment-random-coffee](https://app.slack.com/client/T02592416/C02U92R0S3E) - a channel created by the Donut bot for random coffee chats beteween fulfillment team members.
1. [ ] :palm_tree: Add the [Fulfillment calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com&ctz=Europe%2FMadrid) as an additional calendar to Time Off by Deel
   1. Go to Google Calendar and copy the Fulfillment calendar ID: Calendar settings > Integrate calendar > Calendar ID
   1. Open Time Off by Deel app in Slack
   1. From the main dropdown (where you select to see `Your Events`, `Who's Out`, etc) select Settings > Calendar Sync
   1. Click on `Add Calendar` button and paste the Fulfillment calendar ID
   1. Submit it and the Fulfillment calendar ID should appear in the `Additional calendars to include?` section
1. [ ] Add yourself as a reviewer for customers app - [See an example](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/110130).
1. [ ] Go to [Sentry Team Settings page](https://sentry.gitlab.net/settings/gitlab/teams/) and join `#fulfillment` Sentry team to gain access to `customersdot`. To access Sentry Team Settings page you need access to dev.gitlab.org which is part of [role based entitlements](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_development/role_frontend_engineer.md) for engineering. The issue to give access rights was already created by the bot. Check it out and wait till you get access before proceeding with this task.
1. [ ] 👩🏽‍💻👨🏿‍💻 Set up some [pair programming sessions](https://gitlab.com/gitlab-org/fulfillment/meta/-/blob/master/docs/pair_programming.md) with engineers from Fulfillment
      1. [ ] Pair programming with ___
      1. [ ] Pair programming with ___
      1. [ ] Pair programming with ___
1. [ ] Review the below docs or bookmark them for later. Consider updating the docs if they are outdated or if you find any useful information missing.
      1. [Fulfillment terminologies](https://gitlab.com/gitlab-org/fulfillment/meta/-/blob/master/docs/tips_and_tricks/terminology.md?ref_type=heads)
      1. [Zuora tips and tricks](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/zuora/zuora_tips_and_tricks.md)
      1. [Development and debugging tips](https://gitlab.com/gitlab-org/fulfillment/meta/-/blob/master/docs/tips_and_tricks/developer.md?ref_type=heads)
1. [ ] Ready to pick something to work on? During your first weeks, [you can pick anything that is onboarding from the board](https://gitlab.com/dashboard/issues?scope=all&state=opened&label_name[]=devops%3A%3Afulfillment&label_name[]=onboarding). Make sure you assign yourself to the issue, apply the [right workflow labels](https://about.gitlab.com/handbook/engineering/development/fulfillment/#workflow) and the current milestone. Ping your onboarding buddy or your manager for any help.
1. [ ] Please review [this onboarding issue](https://gitlab.com/gitlab-org/fulfillment-meta/-/blob/master/.gitlab/issue_templates/onboarding.md) and update the template with some improvements as you see fit to make it easier for the next newbie!

## Manager

1. [ ] Add team member [to the Fulfillment calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com&ctz=Europe%2FMadrid)
1. [ ] Add team member to the retrospective (update [membership](https://gitlab.com/gl-retrospectives/fulfillment) and [config file](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml)) -
1. [ ] Add team member to the daily Geekbot updates
1. [ ] Set up 1:1s with the new team member
1. [ ] Update team page with the right team and manager - [here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52673)
1. [ ] Request a manager update in BambooHR
1. [ ] Add team member to [`@fulfillment-group`](https://gitlab.com/fulfillment-group) on GitLab.com
1. [ ] Add team member to [fulfillment-be](https://groups.google.com/a/gitlab.com/g/fulfillment-be) or [fulfillment-fe](https://groups.google.com/a/gitlab.com/g/fulfillment-fe) Google group
1. [ ] @jeromezng to add team member to the [`@fulfillment-engineering`](https://gitlab.com/fulfillment-engineering) Google group (for GCP log access)

## Onboarding buddy

1. [ ] Setup initial Zoom call within first week for coffee chat
1. [ ] Invite team member to #fulfillment-social Slack channel
1. [ ] Provide help as an admin to get Zuora setup during Installation
1. [ ] Key points to discuss in follow up
   1. [ ] Customer application Database & Integration (orders, customers, etc)
   1. [ ] Zuora Subscriptions & Callbacks
   1. [ ] Amendment Service
   1. [ ] Salesforce use and its integration with application
   1. [ ] True up, Renewals & Discounts
   1. [ ] CI minutes & Extra CI minutes workflow
   1. [ ] Unleash & Deployment process
