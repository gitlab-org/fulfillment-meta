## Project handover

<!--
Instructions:

1. Title format: "Fulfillment Project Handover: <Project name>"
1. Fill out the sections below and update references to milestone, epic, and/or related issue(s) at the bottom
1. Create issue

Helpful considerations:

- Try to avoid handing over projects whenever possible as it often leads to problems. See this retrospective for more information: https://gitlab.com/gitlab-org/fulfillment/meta/-/issues/1561#note_1665478980.
- Use this template to:
  - create transparency.
  - provide an opportunity for onboarding team members to ask questions.
  - ensure offboarding team members support in a smooth transition.
-->

_We should try to avoid handing over projects whenever possible. If this is still necessary, please use this template to capture transition items._

## Project main epic/issue and description

- Epic:

<!-- List references to information describing the extent of the project -->

## Involved team members

### Assignees offboarding

<!-- List any assignees currently involved with the project that are moving to other efforts -->
1. PM:
1. EM:

### Assignees onboarding

<!-- List any assignees that will take over the project efforts -->
1. PM:
1. EM:

## Technical resources

<!-- List any technical breakdowns or technical documentation that relate to the project -->

## Completed work (including previous iterations)

<!-- List already completed work that relates to the project -->

## Ongoing work

<!-- List ongoing work that relates to the project and who is responsible for completing it -->

## Remaining work (including planned iterations)

<!-- List work to be completed that relates to the project -->

## Definition of done

<!-- When is this project considered to be completed? Consider feature flag rollouts and removals as well as support and customer facing documentation. -->

## Test plan

<!-- List issues or references to descriptions of the proposed test plan -->

## Rollout plan

<!-- List issues or references to descriptions of the rollout plan tied to the project -->

/milestone [milestone]
/epic [main project epic]
/relate [main issue]
/assign me
