# Fulfillment Group Project

Welcome! This project supports non-development activities of the [Fulfillment Sub-department](https://about.gitlab.com/handbook/engineering/development/fulfillment/). The Fulfillment Sub-department is composed of multiple groups and operates as [Section](https://about.gitlab.com/handbook/product/categories/#fulfillment-section) under Development.

To view what's coming up next, please check out the [Fulfillment issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Afulfillment). You can also view the [direction](https://about.gitlab.com/direction/fulfillment/) page for more info on what Fulfillment covers.

## Filing GitLab CE/EE Issues 

If you'd like to file an issue for GitLab CE or EE that the Fulfillment team should 
look into, please file an issue in the appropriate project:

- [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce)
- [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee)

Please use the ~"section::fulfillment" and ~"devops::fulfillment" labels so we can find it quickly! Where possible, please also assign the correct category label(s) if there are any. 

## Resources

- [Fulfillment team calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com)
- [All Fulfillment Issues](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=devops%3A%3Afulfillment&scope=all&sort=created_date&state=opened&utf8=%E2%9C%93)
- [Fulfillment Security Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=security&label_name[]=devops%3A%3Afulfillment)
- [All Open Fulfillment Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Afulfillment)
- [Product Section Direction - Fulfillment | GitLab](https://about.gitlab.com/direction/fulfillment/)
- [Fulfillment Guide | GitLab](https://about.gitlab.com/handbook/product/fulfillment-guide/#customersdot-admin-panel)
- [#s_fulfillment Slack Channel](https://gitlab.slack.com/archives/CMJ8JR0RH)
